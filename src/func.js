const {listOfPosts, listOfPosts2} = require('../src/posts');

const getSum = (str1, str2) => {
  let isNum1 = /^\d+$/.test(str1);
  let isNum2 = /^\d+$/.test(str2);
  if((typeof str1, typeof str2) === "string" && (isNum1 || isNum2)) return (+str1 + +str2).toString()
  else return false
};  

// console.log(getSum('123maxim', '3coding24'))

const getQuantityPostsByAuthor = (listOfPosts1, authorName) => {
  const posts = listOfPosts1.filter(obj => obj.author === authorName).length;
  const comments = listOfPosts1.reduce((countComments, obj) => 
    (countComments + (obj.comments === undefined ? 0 : obj.comments.filter(comment => comment.author === authorName).length)), 0);
  return `Post:${posts},comments:${comments}`;
};

// console.log(getQuantityPostsByAuthor(listOfPosts, 'Rimus'))


const tickets = (people) => {
  const arrOfNum = people.map(i => Number(i));
  const maxEl = Math.max(...arrOfNum);
  const arr = arrOfNum.filter(number => number !== maxEl)
  const sum = arr.reduce((a,b) => a < maxEl ? a + b : a, 0);
  if(sum % 100 === 0) return 'YES';
  else return 'NO';
};

console.log(tickets([25, 50, 25, 50, 25, 50, 25, 100]))

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
